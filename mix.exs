defmodule Mandelbrot.MixProject do
  use Mix.Project

  def project do
    [
      app: :mandelbrot,
      version: "0.1.0",
      elixir: "~> 1.8",
      build_embedded: true,
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Mandelbrot, []},
      extra_applications: []
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:scenic, "~> 0.10"},
      {:scenic_driver_glfw, "~> 0.10", targets: :host},
      {:exsync, git: "https://github.com/Arkham/exsync", branch: "support-escher", only: :dev}
    ]
  end
end
