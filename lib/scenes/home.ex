defmodule Mandelbrot.Scene.Home do
  use Scenic.Scene
  require Logger

  alias Scenic.Graph
  alias Scenic.ViewPort

  import Scenic.Primitives
  # import Scenic.Components

  @note """
    This is a starter application.
  """

  @text_size 24

  # ============================================================================
  # setup

  # --------------------------------------------------------
  def init(_, opts) do
    Process.register(self(), __MODULE__)

    graph = build_graph(opts)

    {:ok, graph, push: graph}
  end

  def build_graph(opts) do
    {:ok, %ViewPort.Status{size: {width, height}}} = ViewPort.info(opts[:viewport])

    Graph.build(font: :roboto, font_size: @text_size)
    |> add_specs_to_graph([
      text_spec(@note, translate: {20, 120}),
      rect_spec({width, height})
    ])
  end

  def handle_input(event, _context, state) do
    Logger.info("Received event: #{inspect(event)}")
    {:noreply, state}
  end
end
