use Mix.Config

IO.puts("[config/dev.exs] configuring exsync...")

config :exsync,
  reload_timeout: 75,
  reload_callback: {GenServer, :call, [Mandelbrot.Refresher, :refresh_home]}
